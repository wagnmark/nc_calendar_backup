#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import caldav
import toml
import logging

if __name__ == "__main__":
    config = toml.load("config.toml")
    loglevel = logging.WARN
    if config["logging"]["level"] == "DEBUG":
        loglevel = logging.DEBUG
    elif config["logging"]["level"] == "INFO":
        loglevel = logging.INFO
    elif config["logging"]["level"] == "ERROR":
        loglevel = logging.ERROR
    logging.basicConfig(level=loglevel)
    logging.info("=== starting calendar backup ===")
    nc_client = caldav.DAVClient(
        url=config["access"]["url"],
        username=config["access"]["user"],
        password=config["access"]["passwd"],
    )
    try:
        my_principal = nc_client.principal()
    except Exception as err:
        logging.error('nextcloud connection error: "{}"'.format(err))
        exit(1)
    for calendar in my_principal.calendars():
        logging.info('backup calendar "{}"'.format(calendar.name))
        with open("{}.ics".format(calendar.name), "wb") as f:
            for event in calendar.events():
                f.write(bytearray(event.load().data, "utf-8"))
