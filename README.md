# Nextcloud calendar backup script

## installation

1) clone the repository

2) install dependencies
```sh
pip install --user -r requirements.txt
```

## configuration

Create a `config.toml` with the required data (url, credentials, logging) based on `config.toml.example`. The credentials should be an app password, which can be found at the nextcloud account settings.

## use

This script will create an `.ics` file for each calendar:

```sh
python main.py
```